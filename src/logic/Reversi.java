package logic;

import gui.UpdateInterface;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;


public class Reversi {
    private final int[][] boardGame; //zawiera kolory pinkow 0- nieprzypisane, 1-czarny 2-bialy
    private final int boardSize;
    private final UpdateInterface updateInterface;

    private int actualPlayer;   // 1 -czarny 2 - bialy
    private int oponnentPlayer;  // 1 -czarny 2 - bialy

    private final List<String> usedFields;  //do endGame()
    private int incorrectMovesCount;

    public Reversi(int boardSize, UpdateInterface updateInterface) {
        this.boardSize = boardSize;
        this.boardGame = new int[boardSize][boardSize];
        this.usedFields = new ArrayList<>(boardSize);
        this.updateInterface = updateInterface;

        boardGame[3][3] = 2; //TODO uniezaleznic od wielkosci gry, bo obecnie na sztywno dla boardSize=8;
        boardGame[3][4] = 1;
        boardGame[4][3] = 1;
        boardGame[4][4] = 2;

        actualPlayer = 1;
        oponnentPlayer = 2;
        incorrectMovesCount = 0;

        updateInterface.updateGUI(boardGame, 1);
        JOptionPane.showMessageDialog(null, "Rozpoczynamy grę !");
    }

    public void makeMove(int row, int col) //row i col clicked
    {
        boolean isMakedMove = false;
        if (boardGame[row][col] == 0) {
            if (checkLeft(row, col)) isMakedMove = true;
            if (checkRight(row, col)) isMakedMove = true;
            if (checkUp(row, col)) isMakedMove = true;
            if (checkDown(row, col)) isMakedMove = true;
            if (checkDiagLeftUp(row, col)) isMakedMove = true;
            if (checkDiagRightUp(row, col)) isMakedMove = true;
            if (checkDiagLeftDown(row, col)) isMakedMove = true;
            if (checkDiagRightDown(row, col)) isMakedMove = true;

            if (isMakedMove) {
                if (actualPlayer == 1) {
                    actualPlayer = 2;
                    oponnentPlayer = 1;
                } else {
                    actualPlayer = 1;
                    oponnentPlayer = 2;
                }

                incorrectMovesCount = 0;
                usedFields.clear();

                updateInterface.updateGUI(boardGame, actualPlayer);
            } else {
                if (!checkEndGame(row, col)) //ruch niedozwolony
                {
                    String message = "Próbowano wykonanć niedozwolony ruch w (" + row + "," + col + ")";
                    JOptionPane.showMessageDialog(null, message);
                }
            }
        } else {
            checkEndGame(row, col);
        }

    }

    private boolean checkLeft(int row, int col) {
        if (col == 0) return false;

        for (int j = col - 1; j >= 0; j--) {
            if (boardGame[row][j] == actualPlayer && boardGame[row][col - 1] == oponnentPlayer) {
                for (int k = col; k > j; k--) //change color
                {
                    boardGame[row][k] = actualPlayer;
                }
                return true;
            }
        }
        return false;
    }

    private boolean checkRight(int row, int col) {
        if (col == boardSize - 1) return false;

        for (int j = col + 1; j < boardSize; j++) {
            if (boardGame[row][j] == actualPlayer && boardGame[row][col + 1] == oponnentPlayer) {

                for (int k = col; k < j; k++) //change color
                {
                    boardGame[row][k] = actualPlayer;
                }
                return true;
            }
        }
        return false;
    }

    private boolean checkDown(int row, int col) {
        if (row == 0) return false;

        for (int j = row - 1; j >= 0; j--) {
            if (boardGame[j][col] == actualPlayer && boardGame[row - 1][col] == oponnentPlayer) {
                for (int k = row; k > j; k--) //change color
                {
                    boardGame[k][col] = actualPlayer;
                }
                return true;
            }
        }
        return false;
    }


    private boolean checkUp(int row, int col) {
        if (row == boardSize - 1) return false;

        for (int j = row + 1; j < boardSize; j++) {
            if (boardGame[j][col] == actualPlayer && boardGame[row + 1][col] == oponnentPlayer) {
                for (int k = row; k < j; k++) //change color
                {
                    boardGame[k][col] = actualPlayer;
                }
                return true;
            }
        }
        return false;
    }


    private boolean checkDiagLeftUp(int row, int col) {
        if (row == 0 || col == 0) return false;

        for (int i = row - 1, j = col - 1; i >= 0 && j >= 0; i--, j--) {
            if (boardGame[i][j] == actualPlayer && boardGame[row - 1][col - 1] == oponnentPlayer) {
                for (int k = col, l = row; k > j; k--, l--) {
                    boardGame[l][k] = actualPlayer;
                }
                return true;
            }
        }
        return false;
    }

    private boolean checkDiagRightUp(int row, int col) {
        if (row == 0 || col == boardSize - 1) return false;

        for (int i = row - 1, j = col + 1; i >= 0 && j < boardSize; i--, j++) {
            if (boardGame[i][j] == actualPlayer && boardGame[row - 1][col + 1] == oponnentPlayer) {
                for (int k = col, l = row; k < j; k++, l--) {
                    boardGame[l][k] = actualPlayer;
                }
                return true;
            }
        }
        return false;
    }

    private boolean checkDiagLeftDown(int row, int col) {
        if (row == boardSize - 1 || col == 0) return false;

        for (int i = row + 1, j = col - 1; i < boardSize && j >= 0; i++, j--) {
            if (boardGame[i][j] == actualPlayer && boardGame[row + 1][col - 1] == oponnentPlayer) {
                for (int k = col, l = row; k > j; k--, l++) {
                    boardGame[l][k] = actualPlayer;
                }
                return true;
            }
        }
        return false;
    }

    private boolean checkDiagRightDown(int row, int col) {
        if (row == boardSize - 1 || col == boardSize - 1) return false;

        for (int i = row + 1, j = col + 1; i < boardSize && j < boardSize; i++, j++) {
            if (boardGame[i][j] == actualPlayer && boardGame[row + 1][col + 1] == oponnentPlayer) {
                for (int k = col, l = row; k < j; k++, l++) {
                    boardGame[l][k] = actualPlayer;
                }
                return true;
            }
        }
        return false;
    }

    private boolean checkEndGame(int row, int col) {
        int countEmptyFields = 0;
        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                if (boardGame[i][j] == 0) countEmptyFields++;
            }
        }

        if (countEmptyFields == 0) {
            String finishGame = "Koniec gry. \n" + (actualPlayer == 1 ? "Wygrał czarny gracz!" : "Wygrał biały gracz !");
            JOptionPane.showMessageDialog(null, finishGame);
            return true;
        }


        if (usedFields.contains(row + "," + col)) {
            return false;
        }

        usedFields.add(row + "," + col);
        incorrectMovesCount++;

        if (countEmptyFields == incorrectMovesCount) {
            String finishGame = "Koniec gry. \n" + (actualPlayer == 1 ? "Wygrał czarny gracz!" : "Wygrał biały gracz !");

            JOptionPane.showMessageDialog(null, finishGame);
        }
        return true;
    }
}
