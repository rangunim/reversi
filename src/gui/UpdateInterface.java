package gui;


public interface UpdateInterface {
    void updateGUI(int[][] pieces, int actualPlayer);
}
