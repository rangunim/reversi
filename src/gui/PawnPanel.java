package gui;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;


public class PawnPanel extends JPanel {
    private final int row;
    private final int col;

    private int color; //0-default 1-black 2- white

    public PawnPanel(int row, int col) {
        this.row = row;
        this.col = col;
        color = 0;
        this.setBackground(new Color(10, 120, 30));
    }

    public int getColor() {
        return color;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public void setColor(int color) {
        this.color = color;
    }


    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        switch (color) {
            case 1:
                g.setColor(Color.BLACK);
                break;
            case 2:
                g.setColor(Color.WHITE);
                break;
            default:
                g.setColor(new Color(10, 120, 30));
                break;
        }
        g.fillOval(5, 5, 50, 50);
    }
}
