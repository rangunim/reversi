package gui;

import logic.Reversi;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class GameFrame implements UpdateInterface {
    private static final int BOARD_SIZE = 8;

    private final JFrame mainFrame;
    private JPanel gamePanel;
    private PawnPanel[][] pawnPanels;

    private JPanel settingsPanel;
    private final DrawLabel actualPlayerColor = new DrawLabel(new Color(10, 120, 30));
    private JLabel totalWhiteLabel;
    private JLabel totalBlackLabel;

    private Reversi reversi;

    public GameFrame() {
        mainFrame = new JFrame("Reversi");
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setSize(640, 480);
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setResizable(false);

        initGamePanel();
        initSettingsPanel();
        mainFrame.setLayout(new BorderLayout());
        mainFrame.getContentPane().add(gamePanel, BorderLayout.CENTER);
        mainFrame.getContentPane().add(settingsPanel, BorderLayout.EAST);

        mainFrame.setVisible(true);
    }


    private void initGamePanel() {
        gamePanel = new JPanel();
        gamePanel.setLayout(new GridLayout(BOARD_SIZE, BOARD_SIZE, 1, 1));

        pawnPanels = new PawnPanel[BOARD_SIZE][BOARD_SIZE];
        PawnPanel temp;
        for (int col = 0; col < BOARD_SIZE; col++) {
            for (int row = 0; row < BOARD_SIZE; row++) {
                temp = new PawnPanel(col, row);
                pawnPanels[col][row] = temp;
                gamePanel.add(temp);

                pawnPanels[col][row].addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        if (reversi != null) {
                            PawnPanel panel = (PawnPanel) e.getSource();
                            reversi.makeMove(panel.getRow(), panel.getCol());
                        }
                    }
                });
            }
        }
    }


    private void initSettingsPanel() {
        settingsPanel = new JPanel();
        settingsPanel.setMinimumSize(new Dimension(100, 200));
        settingsPanel.setLayout(new GridBagLayout());
        settingsPanel.setBackground(new Color(20, 40, 80));

        JButton startButton = new JButton("Rozpocznij grę");
        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                startGame();
            }
        });
        addComponenet2GridBag(startButton, 0, 0, GridBagConstraints.CENTER, 2, settingsPanel);

        JLabel actualPlayerLabel = new JLabel("Aktualny gracz:");
        actualPlayerLabel.setForeground(Color.WHITE);
        addComponenet2GridBag(actualPlayerLabel, 0, 1, GridBagConstraints.WEST, 1, settingsPanel);

        actualPlayerColor.setColor(Color.BLACK);
        addComponenet2GridBag(actualPlayerColor, 1, 1, GridBagConstraints.WEST, 4, settingsPanel);

        JLabel totalHeaderLabel = new JLabel("Liczba pinów");
        totalHeaderLabel.setForeground(Color.WHITE);
        addComponenet2GridBag(totalHeaderLabel, 0, 3, GridBagConstraints.WEST, 1, settingsPanel);

        totalBlackLabel = new JLabel("0");
        totalBlackLabel.setForeground(Color.BLACK);
        addComponenet2GridBag(totalBlackLabel, 0, 4, GridBagConstraints.EAST, 1, settingsPanel);

        totalWhiteLabel = new JLabel("0");
        totalWhiteLabel.setForeground(Color.WHITE);
        addComponenet2GridBag(totalWhiteLabel, 1, 4, GridBagConstraints.WEST, 1, settingsPanel);

        // JButton showAvaiableMovesButton = new JButton("Pokaz mozliwe ruchy"); //TODO
        //addComponenet2GridBag(showAvaiableMovesButton,0,5,GridBagConstraints.WEST, 2 , settingsPanel);
    }

    private void addComponenet2GridBag(Component component, int gridX, int gridY, int constraint, int gridwidth, JPanel panel) {
        addComponenet2GridBag(component, gridX, gridY, constraint, gridwidth, 1, 0, 0, panel);
    }

    private void addComponenet2GridBag(Component component, int gridX, int gridY, int constraint, int gridwidth, int gridheight, int weigthx, int weigthy, JPanel panel) {
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = gridX;
        gridBagConstraints.gridy = gridY;
        gridBagConstraints.anchor = constraint;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        gridBagConstraints.gridwidth = gridwidth;
        gridBagConstraints.gridheight = gridheight;
        gridBagConstraints.weightx = weigthx;
        gridBagConstraints.weighty = weigthy;
        panel.add(component, gridBagConstraints);
    }

    public void startGame() {
        reversi = new Reversi(BOARD_SIZE, this);
    }

    @Override
    public void updateGUI(int[][] pieces, int actualPlayer) {
        int totalBlack = 0;
        int totalWhite = 0;
        for (int col = 0; col < BOARD_SIZE; col++) {
            for (int row = 0; row < BOARD_SIZE; row++) {
                pawnPanels[col][row].setColor(pieces[col][row]);

                if (pieces[col][row] == 1) totalBlack++;
                else if (pieces[col][row] == 2) totalWhite++;
            }
        }
        totalBlackLabel.setText(totalBlack + "");
        totalWhiteLabel.setText(totalWhite + "");
        gamePanel.repaint();

        if (actualPlayer == 1)
            actualPlayerColor.setColor(Color.BLACK);
        else
            actualPlayerColor.setColor(Color.WHITE);

        actualPlayerColor.repaint();
    }

    class DrawLabel extends JLabel {
        private Color color;

        public DrawLabel(Color c) {
            this.color = c;
            this.setText("XXXXX");
        }

        public Color getColor() {
            return color;
        }

        public void setColor(Color color) {
            this.color = color;
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.setColor(color);
            g.fillRect(0, 0, 80, 800);
        }
    }
}
